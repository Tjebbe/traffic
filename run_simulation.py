from JunktionSimulation import JunktionSimulation
from SWI_Traffic_Objects import SignalGroup
from PlotResults import PlotResults
import json, datetime, os, errno, sys

# Make folder for the output
datestamp = datetime.datetime.now().strftime("%d_%b_%Y_(%Hh%M)")
outputfolder = 'output/output_{}/'.format(datestamp)
try:
    os.makedirs(outputfolder)
except OSError as e:
    if e.errno != errno.EEXIST:
        raise

# Read junction layout from json file.
with open('input/junction.json','r') as f:
   junctionData = json.load(f)
   
sgIDs = junctionData["sgIDs"]

# Read scenario from input json files.
# Alternatively one could make de input files .py files and just import the dictionaries directly.
with open('input/scenario1.json', 'r') as f:
    scenario1 = json.load(f)
with open('input/scenario2.json', 'r') as f:
    scenario2 = json.load(f)
with open('input/scenario3.json', 'r') as f:
    scenario3 = json.load(f)
with open('input/scenario4.json', 'r') as f:
    scenario4 = json.load(f)
with open('input/scenario5.json', 'r') as f:
    scenario5 = json.load(f)

scenarios = [scenario1, scenario2, scenario3, scenario4, scenario5]

# Read arrival times and time it takes to enter and leave sg when light is green
with open('input/arrivalTimes.json', 'r') as f:
    arrivalTimes = json.load(f)

print('running...', end='')
sys.stdout.flush()
i = 0
for scenario in scenarios:
    # make signal groups and add them to the junction list
    # note that after one scenario the sg still has its history of the previous.
    # here we overwrite it (that is why this is in the for loop; maybe improve later).
    junction = []
    for sgID in sgIDs:
        sg = SignalGroup(sgID, arrivalTimes[sgID]["travelTime"], arrivalTimes[sgID]["arrivalTimes"])
        # add signalgroups to junction
        junction.append(sg)
    
    # make class for simulation
    sim = JunktionSimulation()

    # run simulation
    RunTime = 60        # run time in seconds
    res = sim.simulate(RunTime, junction, scenario, verbose=False)
    
    #write output to file
    with open(outputfolder + 'output.txt','a+') as f:
        f.write('Senario {}'.format(i+1))
        f.write(' ({})\n\n'.format(scenario["description"]))
        sumTotalDelay = 0
        for sg in junction:
             f.write('Total delay at signal group {}: {:5.2f} sec\n'.format(sg.identifier, res[sg.identifier].totalDelay))
             sumTotalDelay += res[sg.identifier].totalDelay
        f.write('-------------------------------------------- +\n')
        f.write('                                 {:5.2f} sec\n'.format(sumTotalDelay))
        f.write('\n')
        sumQuadraticDelay = 0
        for sg in junction:
             f.write('Quadratic delay at signal group {}: {:5.2f} sec^2\n'.format(sg.identifier, res[sg.identifier].quadraticDelay))
             sumQuadraticDelay += res[sg.identifier].quadraticDelay
        f.write('----------------------------------------------------- +\n')
        f.write('                                          {:5.2f} sec^2\n'.format(sumQuadraticDelay))
        f.write('\n')
    #plots
    plot = PlotResults(res) 
    plot.plotQueueLengthVsTime(outputfolder + 'scenario_{}.pdf'.format(str(i+1))) # save plots
    # plot.plotQueueLengthVsTime()  # show plots

    i += 1
print('finished!')
