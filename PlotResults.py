from numpy import NaN, isnan, array, concatenate
import matplotlib.pyplot as plt

class PlotResults:

    def __init__(self, res):
        self.res = res
        self.numberOfResults = len(self.res)
    
    def plotQueueLengthVsTime(self, figname = None):
        '''
        plot steps of queue length versus time
        '''
        fig = plt.figure(figsize=(10,8))
        plt.rcParams.update({
            'font.size'  : 18,
            # 'text.usetex': 1,
            # 'font.family':'serif',
            # 'font.serif' :'Computer Modern Typewriter'
            })

        i = 0
        for result_id,result in self.res.items():
            
            lists_sgl = sorted(result.numberSimBicyclesPlot.items()) # sorted by key, return a list of tuples
            t_sgl, sgls = zip(*lists_sgl) # unpack a list of pairs into two tuples


            lists_ql = sorted(result.queueLengthPlot.items()) # sorted by key, return a list of tuples
            t_ql, qls = zip(*lists_ql) # unpack a list of pairs into two tuples

            lists_color = sorted(result.trafficlightPlot.items()) # sorted by key, return a list of tuples
            t_color, colors = zip(*lists_color) # unpack a list of pairs into two tuples
            
            # assert set(t_color) <= set(t_ql), 't_color is not part of t_ql'
            assert t_color == t_ql, 't_color is not the same as t_ql'
            t = array(t_ql)

            red   = array([1 if c == 0 else NaN for c in colors])
            amber = array([1 if c == 1 else NaN for c in colors])
            green = array([1 if c == 2 else NaN for c in colors])
            red_indx   = ~isnan(red)
            amber_indx = ~isnan(amber)
            green_indx = ~isnan(green)   
            
            qls = array(qls)
            sgls = array(sgls)
            colors = array(colors)

            ymax = max(sgls)
            # assert ymax > 0, 'ymax is zero'
            
            # ax = plt.subplot(self.numberOfResults*100 + 10 + i+1)
            ax = plt.subplot(self.numberOfResults*100 + 10 + (self.numberOfResults - i))
            # ax.set_title(result_id)
            ax.plot(t,qls, drawstyle='steps-post',color='k', linewidth = 2)
            
            # avoid to show flushing to calculate totalDelay
            sgls = concatenate((sgls[:-1],[sgls[-2]]))
            ax.fill_between(t,[0]*len(sgls),sgls,color='lightgray')

            ax.plot(t[red_indx]  ,qls[red_indx]  ,'o', color='crimson'    , markersize=10)
            ax.plot(t[amber_indx],qls[amber_indx],'o', color='darkorange' , markersize=10)
            ax.plot(t[green_indx],qls[green_indx],'o', color='darkgreen'  , markersize=10)

            ax.set_ylim([0,1.05*ymax])
            # ax.xticks(t)
            # ax.ylabel(u"queue length \U0001F6B2")

            # if i == self.numberOfResults - 1:
            if i == 0:
                ax.set_xlabel('time [s]')
            ax.set_ylabel("queue length [bikes]")
            i+=1
        
        if figname == None:
            plt.tight_layout()
            plt.show()
        else:
            # fig.savefig('output/senario_{}.pdf'.format(figname),format='pdf',bbox_inches='tight')
            fig.savefig(figname,format='pdf',bbox_inches='tight')
    