from scipy import stats
from collections import deque
# from dist.Distribution import Distribution
from ggcQL.Event import Event
from ggcQL.FES import FES
from ggcQL.SimResults import SimResults
from SWI_Traffic_Objects import SignalGroup, Bicycle

class JunktionSimulation :
    
    def importTrafficLightEvents(self, fes, sg, times_turn_green, times_turn_amber, times_turn_red):
        """
        add traffic light events of signal group sg to event list fes 
        """
        numGreens = len(times_turn_green)
        numAmbers = len(times_turn_amber)
        numReds = len(times_turn_red)
        
        # initialize traffic light changes
        for i in range(numGreens):
            newEvent = Event(Event.TOGREEN, times_turn_green[i], sg) 
            fes.add(newEvent)
        for i in range(numAmbers):
            newEvent = Event(Event.TOAMBER, times_turn_amber[i], sg) 
            fes.add(newEvent)
        for i in range(numReds):
            newEvent = Event(Event.TORED, times_turn_red[i], sg) 
            fes.add(newEvent)
            
        
    def simulate(self, T, junction, scenario, verbose=False):
        """
        main simulation
        """
        t = 0                # current time
        smalldelay = 1e-2    # to schedule events 'now', but still after the current one
        
        fes = FES()

        res = {}  # results keys are identifier of sg
        
        for sg in junction:

            # add traffic light events of signal group sg to event list fes
            times_turn_green = scenario[sg.identifier]['times_turn_green']
            times_turn_amber = scenario[sg.identifier]['times_turn_amber']
            times_turn_red   = scenario[sg.identifier]['times_turn_red']
            self.importTrafficLightEvents(fes,sg, times_turn_green, times_turn_amber, times_turn_red)
            
            sg.initializeSimulatedBikes()      # copy real bikes to sim bikes

            res[sg.identifier] = SimResults()  # class for results

            fes.add(sg.initialEvent(t))        # add initial event to fes
            
            # add closig event at time T
            closingEventAttimeT = Event(Event.STOP_SIMULATION,T,sg)
            fes.add(closingEventAttimeT)
           
        while any([sg.finished == False for sg in junction]):
            e = fes.next()                       # jump to next event
            t = e.time                           # time of event      
            sg = e.sg
            
            if verbose:                            # signal group of event   
                print('---------------------------------------')
                print('new event: ', end='')
                print(e)
                print('before event: number of sim bikes = {}'.format(sg.numberSimBicycles()))
                print('before event: queue length = {}'.format(sg.queueLength()))
                print('before event: traffic color = {}'.format(sg.currentTrafficLight()))

            if e.type == Event.ARRIVAL_AT_SG :
                # event is an arrival at signal group:
                assert False, 'event is arrival to signal group (not allowed for now!)'

            if e.type == Event.ARRIVAL_AT_QUEUE :      
                # event is an arrival at queue
                
                bicycle = e.bicycle                    # bicycle from event
                if sg.queueLength() == 0 and sg.currentTrafficLight() in [sg.GREEN,sg.AMBER]:
                    # if queue lenght is zero, dont add to queue but drive through
                    # schedule departure from signal group
                    dep = Event(Event.DEPARTURE_FROM_SG, t + smalldelay, sg, bicycle)
                    fes.add(dep)
                else:
                    # else add to queue
                    sg.addBicycleToQueue(bicycle)          # add bicycle to queue
                    
                    nextBikeInSignalGroup = sg.nextBicycle(bicycle)
                    if nextBikeInSignalGroup != None:
                        # if there are more bicycles in sg schedule the next arrival to queue
                        timeToNewEvent = max(0,sg.travelTime - (t-nextBikeInSignalGroup.arrivalTime))

                        arrival = Event(Event.ARRIVAL_AT_QUEUE, t + timeToNewEvent + smalldelay , sg, nextBikeInSignalGroup)
                        fes.add(arrival)

            elif e.type == Event.DEPARTURE_FROM_SG :           
                # event is departure from signal group
                
                # remove bicycle from signal group
                removedBicycle = sg.removeFirstBicycle(t)      
                # check if bicycle of event is in front of queue
                assert (e.bicycle == None or e.bicycle == removedBicycle), 'removed bicycle is not the first one'   

                # add delay of removed bicycle to total delay
                res[sg.identifier].registerTotalDelay(removedBicycle)         


            elif e.type == Event.DEPARTURE_FROM_QUEUE :             
                # event is departure
                
                if (sg.currentTrafficLight() == sg.GREEN):
                    # remove bicycle from queue
                    removedBicycle = sg.removeFirstBicycleInQueue(t) 
                    # check if bicycle of event is in front of queue           
                    assert (e.bicycle == None or e.bicycle == removedBicycle), 'removed bicycle is not the first one' 
                
                    # schedule departure from signal group
                    dep = Event(Event.DEPARTURE_FROM_SG, t + smalldelay, sg, removedBicycle)
                    fes.add(dep)
                if (
                    sg.queueLength() > 0
                    and (sg.currentTrafficLight() == sg.GREEN)
                ):
                    # as long as the queue is nonempty and the light is green
                    # make event for departure at time t + reaction time of the bicycles
                    nextBicycleInQueue = sg.getFirstBicycleInQueue()
                    dep = Event(Event.DEPARTURE_FROM_QUEUE, t + Bicycle.reactionTime, sg, nextBicycleInQueue)
                    fes.add(dep)


            elif e.type == Event.TOGREEN:              
                # event is traffic light changed to green
                
                sg.changeTrafficLight(sg.GREEN,t)      # change trafficlight to green

                if sg.queueLength() > 0:
                    # if light turns green and bicycles are waiting schedule departure
                    dep = Event(Event.DEPARTURE_FROM_QUEUE, t + Bicycle.reactionTime,sg)
                    fes.add(dep)
                
            elif e.type == Event.TORED:               # event is traffic light changed to red
                sg.changeTrafficLight(sg.RED,t)       # change trafficlight to red
            
            elif e.type == Event.TOAMBER:             # event is traffic light changed to amber
                sg.changeTrafficLight(sg.AMBER,t)     # change trafficlight to amber
                
            elif e.type == Event.STOP_SIMULATION:     # simulation end reached
                # remove all bikes from signal-group (which calculates the delays), add a delay if they are in a queue and add their delay
                # ATTENTION: not feasible if the simulation should be continued afterwards with a warm start
                cumulativeDelay = 0 #addded up reaction times for bikes in the queue
                while sg.numberSimBicycles() > 0:
                    removedBicycle = sg.removeFirstBicycle(t)
                    firstBikeInQueue = sg.getFirstBicycleInQueue()
                    if removedBicycle == firstBikeInQueue:
                        cumulativeDelay += Bicycle.reactionTime
                        removedBicycle.delay += cumulativeDelay
                        sg.removeFirstBicycleInQueue
                    
                    res[sg.identifier].registerTotalDelay(removedBicycle)
                
                # set signal flag to finish
                sg.finished = True

            
            if verbose:
                print('after event: number of sim bikes = {}'.format(sg.numberSimBicycles()))
                print('after event: queue length = {}'.format(sg.queueLength()))
                print('after event: traffic color = {}'.format(sg.currentTrafficLight()))
        
            res[sg.identifier].registerSignalGroupLength(t, sg) # register the number of bikes in signal group at time t
            res[sg.identifier].registerQueueLength(t, sg)       # register the queue length after event at time t
            res[sg.identifier].registerTrafficLightColor(t,sg)  # register traffic light color after event at time t
        
        return res
                          
        
        
        
        
        
