# Based on code of lecture notes: Stochastic Simulation using Python by Mark Boon a.o.

class Event:

    TORED   = 0   # constant for change of traffic-light to RED
    TOAMBER = 1   # constant for change of traffic-light to AMBER
    TOGREEN = 2   # constant for change of traffic-light to GREEN
    
    ARRIVAL_AT_SG        = 3     # constant for arrival type
    ARRIVAL_AT_QUEUE     = 4     # constant for arrival at queue type
    DEPARTURE_FROM_SG    = 5     # constant for departure type
    DEPARTURE_FROM_QUEUE = 6     # constant for departure from queue type

    STOP_SIMULATION = 7          # stop simulation
    
    def __init__(self, typ, time, sg, bicycle = None):  # type is a reserved word
        self.type = typ                 # type of event
        self.time = time                # time of event
        self.sg   = sg                  # signal group where this events takes place
        self.bicycle  = bicycle                 # bicycle of event (optional)
        
    def __lt__(self, other):            # compare to other events 
        return self.time < other.time
    
    def __str__(self):      
        s = (
            'ToRed',
            'ToAmber',
            'ToGreen',
            'Arrival for sg',
            'Arrival at queue',
            'Departure from sg',
            'Departure from queue',
            'Derminate simulation'
        )
        string = '{:20s} at t = {:3.2f} of {} '.format(s[self.type],self.time, self.sg)
        if self.bicycle:
            string += 'of {}'.format(self.bicycle)
        return string
    