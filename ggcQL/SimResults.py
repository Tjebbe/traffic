# Based on code of lecture notes: Stochastic Simulation using Python by Mark Boon a.o.

from numpy.ma.core import zeros

class SimResults:
    
    MAX_QL = 1000
    
    def __init__(self):
        
        self.trafficlightPlot = {}           # traffic light color vs time
        self.queueLengthPlot  = {}           # queue lengths vs event time
        self.numberSimBicyclesPlot  = {}     # number of bikes in the sg vs event time

        self.queueLengthHistogram = zeros(self.MAX_QL + 1)  # histogram output
        self.oldTime = 0       # save time of previous event for histogram. 
        
        self.totalDelay = 0    # total delay
        self.quadraticDelay = 0   # total quadratic delay cost
        self.sumQL = 0

    def registerQueueLength(self, t, sg):
        '''
        t is time 
        sg is signal group

        saves the queuelength of the signal group at time t
        saves histogram that shows distribution of queuelength vs time
        '''
        ql = sg.queueLength()               # queue length of signal group

        self.queueLengthPlot[t] = ql        # save queue length at time t
        
        self.queueLengthHistogram[min(ql, self.MAX_QL)] += (t - self.oldTime)
        self.sumQL += (t - self.oldTime) * ql
        self.oldTime = t         # save previous time
    
    def registerSignalGroupLength(self, t, sg):
        '''
        t is time 
        sg is signal group

        saves the number of bicycles in the signal group at time t
        '''
        ql = sg.numberSimBicycles()               # queue length of signal group

        self.numberSimBicyclesPlot[t] = ql        # save queue length at time t
        # self.queueLengthHistogram[min(ql, self.MAX_QL)] += (t - self.oldTime)
        # self.oldTime = t         # save previous time
    
    def registerTrafficLightColor(self, t, sg):
        '''
        t is time 
        sg is signal group

        saves the current traffic light color of the signal group at time t
        '''
        color = sg.currentTrafficLight()        # queue length of signal group
        self.trafficlightPlot[t] = color        # save queue length at time t
        
    def registerTotalDelay(self, bicycle):
        '''
        bicycle is a bicycle

        addes delay of individual bicycle to total delay (and to quadratic cost as well)
        '''
        self.totalDelay += bicycle.delay
        self.quadraticDelay += bicycle.delay * bicycle.delay  # quadratic delay


    def getMeanQueueLength(self): 
        return self.sumQL / self.oldTime
    
    def getQueueLengthProbabilities(self) :
        return [x/self.oldTime for x in self.queueLengthHistogram]

    def plotQueueLengthHistogram(self, maxq=25):
        ql = self.getQueueLengthProbabilities()
        maxx = maxq + 1
        plt.figure()
        plt.bar(range(0, maxx), ql[0:maxx])
        plt.ylabel('P(Q = k)')
        plt.xlabel('k')
        plt.show()
    
    # def plotQueueLengthVsTime(self):
    #     '''
    #     plot steps of queue length versus time
    #     '''
        
    #     lists_sgl = sorted(self.numberSimBicyclesPlot.items()) # sorted by key, return a list of tuples
    #     t_sgl, sgls = zip(*lists_sgl) # unpack a list of pairs into two tuples


    #     lists_ql = sorted(self.queueLengthPlot.items()) # sorted by key, return a list of tuples
    #     t_ql, qls = zip(*lists_ql) # unpack a list of pairs into two tuples

    #     lists_color = sorted(self.trafficlightPlot.items()) # sorted by key, return a list of tuples
    #     t_color, colors = zip(*lists_color) # unpack a list of pairs into two tuples
        
    #     # assert set(t_color) <= set(t_ql), 't_color is not part of t_ql'
    #     assert t_color == t_ql, 't_color is not the same as t_ql'
    #     t = array(t_ql)

    #     red   = array([1 if c == 0 else NaN for c in colors])
    #     amber = array([1 if c == 1 else NaN for c in colors])
    #     green = array([1 if c == 2 else NaN for c in colors])
    #     red_indx   = ~isnan(red)
    #     amber_indx = ~isnan(amber)
    #     green_indx = ~isnan(green)   
        
    #     qls = array(qls)
    #     sgls = array(sgls)
    #     colors = array(colors)

    #     ymax = max(sgls)
    #     # assert ymax > 0, 'ymax is zero'
    #     plt.figure()
    #     plt.plot(t,qls, drawstyle='steps-post',color='k', linewidth = 2)
    #     # plt.fill_between(t,[0]*len(sgls),sgls,color='#373F51')
        
    #     # avoid to show flushing to calculate totalDelay
    #     sgls = concatenate((sgls[:-1],[sgls[-2]]))
    #     plt.fill_between(t,[0]*len(sgls),sgls,color='lightgray')

    #     plt.plot(t[red_indx]  ,qls[red_indx]  ,'o', color='crimson'    , markersize=10)
    #     plt.plot(t[amber_indx],qls[amber_indx],'o', color='darkorange' , markersize=10)
    #     plt.plot(t[green_indx],qls[green_indx],'o', color='darkgreen'  , markersize=10)

    #     plt.ylim([0,1.05*ymax])
    #     # plt.xticks(t)
    #     plt.xlabel('time [s]')
    #     # plt.ylabel(u"queue length \U0001F6B2")
    #     plt.ylabel("queue length [bikes]")
    #     plt.show()

    # def __str__(self):
    #     ql = self.getQueueLengthProbabilities()
    #     s = 'Mean queue length: ' + str(self.getMeanQueueLength()) + '\n'
    #     s += 'Queue-length probabilities: \n'
    #     for i in range(21):
    #         s += 'P(Q = ' + str(i) + ') = ' + str(ql[i]) + '\n'
    #     return s

# t = [1, 2, 3.5, 4, 4.5, 5]
# q = [4, 5, 5  , 4, 4  , 4]
# c = [0, 0, 2  , 2, 1  , 0]

# pif = SimResults()
# for i in range(len(t)):
#     pif.queueLengthPlot[t[i]] = q[i]
#     pif.trafficlightPlot[t[i]] = c[i]

# pif.plotQueueLengthVsTime()

