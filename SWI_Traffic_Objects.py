from collections import deque
from ggcQL.Event import Event

class Bicycle:
    """
    class for bicycle
    or C02 neutral car if you want
    """

    reactionTime = 3  # time the bicycle needs to start driving after seeing the light change to green

    def __init__(self,identifier,arrivalTimeGiven):
        assert type(identifier)==str
         
        self.identifier = identifier         # name of bicycle
        self.arrivalTime = arrivalTimeGiven  # arrival time of the bicycle
        self.delay = 0                       # delay of this bicycle

    def __str__(self):
        return self.identifier
                  
class SignalGroup:

    RED   = 0
    AMBER = 1
    GREEN = 2
        
    def __init__(self,identifier,travelTimeGiven,arrivalTimes):
        assert type(identifier)==str
        
        # public
        self.identifier = identifier          # string
        self.travelTime = travelTimeGiven     # time needed to drive from first loop to exit
        self.realBicycles = deque()           # list of Bicycles that are physcially in the signal group based on sensor data
        self.finished = False                 # flag to check if sg simulation is finished

        #private
        self._simulatedBicycles = deque()      # (empty) list of Bicycles that are simulated
        self._trafficLight = self.RED          # red traffic light
        self._queue = deque()                  # list of Bicycles in a queue
        
        self.importRealBicycles(arrivalTimes)  # convert arrival times to real bikes


    def __str__(self):
        return self.identifier

    def importRealBicycles(self, arrivalTimes):
        '''
        arrivalTimes are arrival times of bikes in past

        fills the list of realBicycles
        '''

        numArrivals = len(arrivalTimes)   # number of bicycles arrive the signal group
        # initialize arrivals
        for i in range(numArrivals):
            self.realBicycles.append(Bicycle('bike{}'.format(i+1),arrivalTimes[i]))


    def initializeSimulatedBikes(self):
        '''
        copy real bikes to sim bikes
        '''
        # copy realBicycles into simulatedCycles
        for bicycle in self.realBicycles:
            newSimBicycle = Bicycle('{}sim{}'.format(self.identifier,bicycle.identifier),bicycle.arrivalTime)
            self._simulatedBicycles.append(newSimBicycle)
        # for bike in self._simulatedBicycles:
        #     print(bike)
    
    def initialEvent(self,currentTime):
        """
        create the initial event
        """

        firstSimBicycle = self._simulatedBicycles[0]
        if self._trafficLight in [self.GREEN]:  
            # To do: make sure it does not pass red (and add self.AMBER)

            # schedule departure event of first Bicycle
            timeToNewEvent = max(Bicycle.reactionTime,self.travelTime - (currentTime-firstSimBicycle.arrivalTime))
            newEvent = Event(Event.DEPARTURE_FROM_SG,currentTime+timeToNewEvent,self,firstSimBicycle)
        else :
            # schedule arrivale at queue list for first Bicycle
            timeToNewEvent = max(0,self.travelTime - (currentTime-firstSimBicycle.arrivalTime))
            newEvent = Event(Event.ARRIVAL_AT_QUEUE,currentTime+timeToNewEvent,self,firstSimBicycle)
        
        return newEvent
       
    def addBicycle(self,bicycle):
        """
        add bicycle to signal group
        """
        if len(self._simulatedBicycles)>0:
            assert bicycle.arrivalTime > self._simulatedBicycles[-1].arrivalTime, 'Trying to add bicycle to signal group which arrived before the last Bicycle already in the signal group'
        self._simulatedBicycles.append(bicycle)
        
    def addBicycleToQueue(self,bicycle):
        """
        add bicycle to queue
        """
        assert bicycle in self._simulatedBicycles, 'Trying to add bicycle which is not in signal group to queue'
        self._queue.append(bicycle)
    
    def queueLength(self):
        """
        return the queue length
        """
        return len(self._queue)
         
    def numberSimBicycles(self):
        """
        return number of bikes in the signal group
        """
        return len(self._simulatedBicycles)
    
    def nextBicycle(self,bike):
        '''
        return the bike after the given bike
        '''
        assert bike in self._simulatedBicycles, 'bicycle is not in signal group'
        if bike == self._simulatedBicycles[-1]:
            # if the bike was the last bike there is no bike after this one
            return None
        else:
            # get index of bike and return the next one.
            index_bike = self._simulatedBicycles.index(bike)
            return self._simulatedBicycles[index_bike+1]

    def removeFirstBicycle(self,time):
        """
        remove and return the first bicycle from the signal group
        """
        firstBicycle = self._simulatedBicycles.popleft()
        firstBicycle.delay = (time - firstBicycle.arrivalTime) - self.travelTime  # time needed - ideal time
        return firstBicycle
    
    def removeFirstBicycleInQueue(self,t):
        """
        remove the first bicycle from the queue
        """
        return self._queue.popleft()

    def getFirstBicycleInQueue(self):
        """
        return the first bike in the queue
        """
        if len(self._queue) == 0:
            return None
        else:
            return self._queue[0]
    
    def changeTrafficLight(self,newTrafficLight,time):
        """
        change the traffic light color at time time
        """
        self._trafficLight = newTrafficLight
         
        # later on we may do more stuff depending on the new value
         
    def currentTrafficLight(self):
        """
        return current traffic light color
        """
        return self._trafficLight
         
# to add later
# class Junction:
#     def __init__(self,identifier):
#         assert type(identifier)==str
#         self.identifier = identifier
#         self.signalGroups = []
          
#     def addSignalGroup(self,signalGroup):
#         self.signalGroups.append(signalGroup)
