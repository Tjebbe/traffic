# traffic

## requirements
- python 3.7.6
- scipy (incl numpy)
- matplotlib


## How to run
- make input files
- run run_simulation.py

## Pseudo code of main simulation
```
T = 60      # length of simulation
t = 0       # starting time

## There are several types of events 

We have the following list:
-TORED, TOAMBER, TOGREEN: these are switches of the traffic light from amber to red, from green to amber and from red to green
-ARRIVAL_AT_SG: the event that a bicycle passes a loop at a considerable (known) distance from the intersection
-ARRIVAL_AT_QUEUE: the event that a bicycle joins the queue at the stop line of the intersection
-DEPARTURE_FROM_SG: the event that a bicycle departs from the signal group and crosses the intersection
-DEPARTURE_FROM_QUEUE: the event that a bicycle departs from the queue (which happens right before DEPARTURE_FROM_SG)

initialize()  # initialize events (fes), create junction with signal groups etc.
We assume that we have/know:
-A layout of the intersection
-All the bicycles still in the signal groups at time t and their time of ARRIVAL_AT_SG; these might directly lead to an ARRIVAL_AT_QUEUE event
-A scenario for the traffic lights
We create all events and add them to the events list (fes)

while (t < T):
    select next event from event list (fes)
    save the time (t) of this event

    if event type == ARRIVAL_AT_SG: ## not needed now, probably needed when looking at a network of intersections
        #create bicycle with arrival time ARRIVAL_AT_SG
        #add bicycle to signal group
    
    if event type == ARRIVAL_AT_QUEUE
        if (
            queue length of signalgroup == 0
            and its current traffic light color == green or amber
            ) :
            # the bicycles can just pass, hence they depart after driving
            # from the loop at the stop line to the traffic light 
            schedule DEPARTURE_FROM_QUEUE at time t
	else 
            add bicycle to the queue
            if (
                there are more bicycles in the signal group that are not in the queue
                ) :
                schedule the event ARRIVAL_AT_QUEUE of the next bicycle
        
    if event type == DEPARTURE_FROM_QUEUE:
        remove first bicycle from queue
        add to delay of the bicycle to the total delay

        if (
            queue length of signalgroup > 0
            and its current traffic light color == green
            ) :
            # there are still bicycles waiting and the light is green 
            schedule DEPARTURE_FROM_QUEUE at time t + reactionTime

        #we also remove the bicycle from the signalgroup
        schedule DEPARTURE_FROM_SG at time t

    if event type == DEPARTURE_FROM_SG:
        we remove the bicycle from the signal group

    if event type == TOGREEN:
        turn light color of signal group to green
        if (
            queue length of signal group > 0
            ) :
            # there are bicycles waiting and the light just became green 
            schedule DEPARTURE_FROM_QUEUE at time t + reactionTime

    if event type == TOAMBER:
        turn light color of signal group to amber

    if event type == TORED:
        turn light color of signal group to red
```



